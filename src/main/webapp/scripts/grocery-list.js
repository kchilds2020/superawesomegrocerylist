//retrieving query params from the url
const urlParams = new URLSearchParams(window.location.search)
const listId = urlParams.get("listId")
const userId = urlParams.get("userId")

//function that gets called when the window loads
window.onload = async function(){

    //check session
    const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionUserData = await sessionRes.json()

    console.log(sessionUserData)
    if(sessionUserData.data){
       if(sessionUserData.data.id != userId){
           window.location = `${domain}/`
       } 
    }else{
        window.location = `${domain}/`
    }

    populateData()
}


let createItemForm = document.getElementById("create-item-form")
//create new element function
createItemForm.onsubmit = async function(e){
    e.preventDefault();


    let itemNameVal = document.getElementById("itemInputField").value;

    let createRes = await fetch(`${domain}/api/item`,{
        method: "POST",
        body: JSON.stringify({
            name: itemNameVal,
            listFk: listId
        })
    })

    let createResData = await createRes.json()

    if(createResData.success){
        populateData()
    }
}

//populate all items given list id
async function populateData(){
    //get all items for grocery list
    let itemsRes = await fetch(`${domain}/api/item?listId=${listId}`)
    let itemData = await itemsRes.json()


    //sort the data
    itemData.data.sort((a,b) => {
        if(a.name < b.name)
            return -1
        if(a.name > b.name)
            return 1;
        
        return 0;    
    })

    let itemContElem = document.getElementById("gi-container")
    itemContElem.innerHTML = ``;

    itemData.data.forEach(item => {
        itemContElem.innerHTML += `
        <div id="grocery-item">
            <div class="item-name" id="item-name${item.id}"><div>${item.name}</div></div>
            <div>
                <button id="inCartBtn${item.id}" onclick="inCartFtn(${item.id})" class="btn btn-success">In Cart</button>
                <button id="deleteBtn${item.id}" onclick="deleteFtn(${item.id})" class="btn btn-danger">Delete</button>
            </div>
        </div>   
        
        `

        let nameElem = document.getElementById(`item-name${item.id}`)

        if(item.inCart){
            nameElem.style = "text-decoration: line-through;"
        }

    })
}


//mark item in cart given id
async function inCartFtn(id){
    let inCartRes = await fetch(`${domain}/api/item?itemId=${id}`,{
        method: "PATCH"
    })

    let inCartData = await inCartRes.json();

    if(inCartData.success){
        populateData()
    }

}


//delete item given id
async function deleteFtn(id){
    console.log("clicked")
            let deleteRes = await fetch(`${domain}/api/item?itemId=${id}`,{
                method: "DELETE"
            })

            let deleteData = await deleteRes.json();

            if(deleteData.success){
                populateData()
            }
}


//go back to dashboard
let dashboardBtn = document.getElementById("dashboard-btn");
dashboardBtn.onclick = function(){
    window.location = `${domain}/dashboard?id=${userId}`
}