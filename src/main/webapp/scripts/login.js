let loginForm = document.getElementById("login-form");

window.onload = async function(){
    const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionUserData = await sessionRes.json();

    if(sessionUserData.data){
        window.location = `${domain}/dashboard?id=${sessionUserData.data.id}`
    }
}


loginForm.onsubmit = async function(e){
    e.preventDefault();

    //get values from the input field
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;

    console.log(username,password)

    //how do we send values to the backend?
    let response = await fetch(`${domain}/api/login`,{
        method: "POST",
        body: JSON.stringify({
            username: username,
            password: password
        })
    })

    let responseData = await response.json();
    console.log(responseData)

    if(responseData.success){
        window.location = `${domain}/dashboard?id=${responseData.data.id}`
    }else{
        let messageElem = document.getElementById("login-message")
        messageElem.style = "background-color: white;"
        messageElem.innerText = responseData.message
    }


}

