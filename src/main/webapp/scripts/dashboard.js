//get id from query param
const urlParams = new URLSearchParams(window.location.search)
const userId = urlParams.get("id")

//function called on window load
window.onload = async function(){
    //check session
    const sessionRes = await fetch(`${domain}/api/check-session`)
    const sessionUserData = await sessionRes.json()

    console.log(sessionUserData)
    if(sessionUserData.data){
       if(sessionUserData.data.id != userId){
           window.location = `${domain}/`
       } 
    }else{
        window.location = `${domain}/`
    }
    populateData();
}

async function populateData(){
    //fetches lists given user id (using ajax)
    const listResponse = await fetch(`${domain}/api/list?id=${userId}`)
    const listData = await listResponse.json();


    //sort the data
    listData.data.sort((a,b) => {
        if(a.name < b.name)
            return -1
        if(a.name > b.name)
            return 1;
        
        return 0;    
    })

    //gets parent element of all cards
    let groceryListElem = document.getElementById("gc-container")
    //remove all past cards
    groceryListElem.innerHTML = ``;

    //iterate through each element in grocery lists for user
    //PURPOSE: create cards in HTML
    listData.data.forEach(list => {
        console.log(list)

        //tag for the base card
        let cardElem = document.createElement("div");
        cardElem.className = "grocery-list-card";

        //tag for the name of grocery list
        let listNameElem = document.createElement("div");
        listNameElem.className = "list-name";
        listNameElem.innerText = list.name;

        //tag container for my buttons
        let btnContElem = document.createElement("div")


        //button to go view items in grocery list
        let viewBtnElem = document.createElement("button")
        viewBtnElem.innerText = "View"
        viewBtnElem.className = "btn btn-primary"
        viewBtnElem.id = "view-btn";

        //button to delete grocery list
        let deleteBtnElem = document.createElement("button")
        deleteBtnElem.innerText = "Delete"
        deleteBtnElem.className = "btn btn-danger"
        deleteBtnElem.id = "delete-btn";



        /* 
            Developing the following structure:

            <div "cardElem">
                <div>list name</div>
                <div>
                    <button "viewBtn">
                    <button "deleteBtn">
                </div>
            </div>    
        */

        //appendChild inserts an element INSIDE of another element.
        btnContElem.appendChild(viewBtnElem)
        btnContElem.appendChild(deleteBtnElem)

        cardElem.appendChild(listNameElem)
        cardElem.appendChild(btnContElem)

        groceryListElem.appendChild(cardElem)



        //function for when view button is clicked
        viewBtnElem.onclick = function(){
            window.location = `${domain}/grocery-list?listId=${list.id}&userId=${userId}`
        }

        //function for when delete button is clicked
        deleteBtnElem.onclick = async function(){
            let deleteRes = await fetch(`${domain}/api/list?id=${list.id}`, {
                method: "DELETE"
            })
            let deleteDataRes = await deleteRes.json()

            if(deleteDataRes.success)
                populateData()
        }

    })
}

//end session and redirect to login when logout button is clicked
let logoutBtn = document.getElementById("logout-btn")
logoutBtn.onclick = async function(){
    let logoutRes = await fetch(`${domain}/api/logout`)

    let logoutResData = await logoutRes.json();

    if(logoutResData.success)
        window.location = `${domain}/`
}



let createListForm = document.getElementById("create-list-form")
//function for creating a new list
createListForm.onsubmit = async function(e){
    e.preventDefault();


    let listNameVal = document.getElementById("listInputField").value;

    let createRes = await fetch(`${domain}/api/list`,{
        method: "POST",
        body: JSON.stringify({
            name: listNameVal,
            userFk: userId
        })
    })

    let createResData = await createRes.json()

    if(createResData.success){
        let listNameElem = document.getElementById("listInputField")
        listNameElem.value = ''
        populateData()
    }

}