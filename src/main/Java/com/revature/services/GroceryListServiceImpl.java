package com.revature.services;

import com.revature.dao.GroceryListDao;
import com.revature.dao.GroceryListDaoImpl;
import com.revature.dao.ItemDao;
import com.revature.dao.ItemDaoImpl;
import com.revature.models.GroceryList;

import java.util.List;

public class GroceryListServiceImpl implements GroceryListService{
    GroceryListDao groceryListDao;
    ItemDao itemDao;

    public GroceryListServiceImpl(){

        groceryListDao = GroceryListDaoImpl.getInstance();
        itemDao = ItemDaoImpl.getInstance();
    }

    @Override
    public void deleteListGivenListId(Integer listId) {

        itemDao.deleteAllItemsGivenListId(listId);
        groceryListDao.deleteListGivenListId(listId);
    }

    @Override
    public List<GroceryList> getAllListsGivenUserId(Integer userId) {
        return groceryListDao.getAllListsGivenUserId(userId);
    }

    @Override
    public void createList(GroceryList list) {
        groceryListDao.createList(list);
    }
}
