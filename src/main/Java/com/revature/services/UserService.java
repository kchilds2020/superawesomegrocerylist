package com.revature.services;

import com.revature.models.User;

public interface UserService {
    User register(User user);
    User login(User user);
}
