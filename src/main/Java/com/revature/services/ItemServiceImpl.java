package com.revature.services;

import com.revature.dao.ItemDao;
import com.revature.dao.ItemDaoImpl;
import com.revature.models.Item;

import java.util.List;

public class ItemServiceImpl implements ItemService{
    ItemDao itemDao;

    public ItemServiceImpl(){
        itemDao = ItemDaoImpl.getInstance();
    }

    @Override
    public void createItem(Item item) {
        itemDao.createItem(item);
    }

    @Override
    public List<Item> getAllItemsGivenListId(Integer listId) {
        return itemDao.getAllItemsGivenListId(listId);
    }

    @Override
    public void deleteItem(Integer itemId) {
        itemDao.deleteItem(itemId);
    }

    @Override
    public void markItemInCart(Integer itemId) {
        itemDao.markItemInCart(itemId);
    }
}
