package com.revature.services;

import com.revature.dao.UserDao;
import com.revature.dao.UserDaoImpl;
import com.revature.models.User;

public class UserServiceImpl implements UserService{
    UserDao userDao;

    public UserServiceImpl(){
        userDao = UserDaoImpl.getInstance();
    }

    @Override
    public User register(User user) {
        //check if username exists in the system
        User tempUser = userDao.getOneUser(user.getUsername());
        if(tempUser != null)
            return null;

        userDao.insertUser(user);

        return userDao.getOneUser(user.getUsername());
    }

    @Override
    public User login(User user) {
        //check username exists in system
        User tempUser = userDao.getOneUser(user.getUsername());
        if(tempUser == null)
            return null;

        //check if password is incorrect
        if(!tempUser.getPassword().equals(user.getPassword()))
            return null;

        return tempUser;
    }
}
