package com.revature.frontcontroller;

import com.revature.controller.GroceryListController;
import com.revature.controller.ItemController;
import com.revature.controller.UserController;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//endpoint routing
@WebServlet(name="dispatcher", urlPatterns = "/api/*")
public class Dispatcher extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String URI = req.getRequestURI();
        System.out.println(URI);

        switch(URI){
            case "/api/login":
                if(req.getMethod().equals("POST"))
                    UserController.getInstance().login(req, resp);
                break;
            case "/api/register":
                if(req.getMethod().equals("POST"))
                    UserController.getInstance().register(req, resp);
                break;
            case "/api/list":
                switch (req.getMethod()){
                    case "GET":
                        GroceryListController.getInstance().getAllLists(req,resp);
                        break;
                    case "POST":
                        GroceryListController.getInstance().createList(req,resp);
                        break;
                    case "DELETE":
                        GroceryListController.getInstance().deleteList(req, resp);
                        break;
                }
                break;
            case "/api/item":
                switch (req.getMethod()){
                    case "GET":
                        ItemController.getInstance().getAllItems(req,resp);
                        break;
                    case "POST":
                        ItemController.getInstance().createItem(req,resp);
                        break;
                    case "PATCH":
                        ItemController.getInstance().markItemInCart(req,resp);
                        break;
                    case "DELETE":
                        ItemController.getInstance().deleteItem(req,resp);
                        break;

                }
                break;
            case "/api/check-session":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().checkSession(req, resp);
                break;
            case "/api/logout":
                if(req.getMethod().equals("GET"))
                    UserController.getInstance().logout(req, resp);
                break;



        }


    }
}
