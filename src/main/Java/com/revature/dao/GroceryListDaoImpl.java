package com.revature.dao;

import com.revature.models.GroceryList;
import com.revature.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

//singleton
public class GroceryListDaoImpl implements GroceryListDao{
    private static GroceryListDao groceryListDao;

    private GroceryListDaoImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static GroceryListDao getInstance(){
        if(groceryListDao == null)
            groceryListDao = new GroceryListDaoImpl();

        return groceryListDao;
    }

    @Override
    public void deleteListGivenListId(Integer listId) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "DELETE FROM lists WHERE list_id = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setInt(1, listId);


            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<GroceryList> getAllListsGivenUserId(Integer userId) {
        List<GroceryList> lists = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "SELECT * FROM lists WHERE user_id_fk = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, userId);

            //this is wh
            ResultSet rs = ps.executeQuery();


            //this is iterating through the records
            while(rs.next()) {
                lists.add(
                    new GroceryList(rs.getInt(1),rs.getString(2),rs.getInt(3))
                );
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return lists;
    }

    @Override
    public void createList(GroceryList list) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "INSERT INTO lists VALUES (DEFAULT, ?, ?)";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setString(1, list.getName());
            ps.setInt(2, list.getUserFk());

            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
