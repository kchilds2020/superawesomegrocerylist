package com.revature.dao;

public class ConnectionUtil {
    public static String url = "jdbc:postgresql://" + System.getenv("TODO_DATABASE_URI") + "/grocerylist_app";
    public static String username = System.getenv("TODO_DATABASE_USERNAME");
    public static String password = System.getenv("TODO_DATABASE_PASSWORD");
}
