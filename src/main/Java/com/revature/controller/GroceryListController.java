package com.revature.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.GroceryList;
import com.revature.models.Response;
import com.revature.models.User;
import com.revature.services.GroceryListService;
import com.revature.services.GroceryListServiceImpl;
import com.revature.services.UserService;
import com.revature.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

public class GroceryListController {
    private static GroceryListController groceryListController;
    GroceryListService groceryListService;
    private GroceryListController(){
        groceryListService = new GroceryListServiceImpl();
    }

    public static GroceryListController getInstance(){
        if(groceryListController == null)
            groceryListController = new GroceryListController();

        return groceryListController;
    }

    public void getAllLists(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer userId = Integer.parseInt(req.getParameter("id"));

        out.println(new ObjectMapper().writeValueAsString(new Response("lists retrieved", true, groceryListService.getAllListsGivenUserId(userId))));

    }

    public void createList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        GroceryList groceryList = new ObjectMapper().readValue(requestBody,GroceryList.class);

        groceryListService.createList(groceryList);

        out.println(new ObjectMapper().writeValueAsString(new Response("list has been created for user id " + groceryList.getUserFk(),true,null)));
    }

    public void deleteList(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer listId = Integer.parseInt(req.getParameter("id"));

        groceryListService.deleteListGivenListId(listId);

        out.println(new ObjectMapper().writeValueAsString(new Response("list deleted", true, null)));

    }


}
