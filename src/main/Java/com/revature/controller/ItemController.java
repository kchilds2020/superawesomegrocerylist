package com.revature.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.GroceryList;
import com.revature.models.Item;
import com.revature.models.Response;
import com.revature.services.ItemService;
import com.revature.services.ItemServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

//singleton
public class ItemController {
    private static ItemController itemController;
    ItemService itemService;
    private ItemController(){
        itemService = new ItemServiceImpl();
    }

    public static ItemController getInstance(){
        if(itemController == null)
            itemController = new ItemController();

        return itemController;
    }

    public void getAllItems(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer listId = Integer.parseInt(req.getParameter("listId"));

        out.println(new ObjectMapper().writeValueAsString(new Response("items retrieved", true, itemService.getAllItemsGivenListId(listId))));

    }

    public void createItem(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Item item = new ObjectMapper().readValue(requestBody,Item.class);

        itemService.createItem(item);

        out.println(new ObjectMapper().writeValueAsString(new Response("item has been created for list id " + item.getListFk(),true,null)));

    }

    public void markItemInCart(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer itemId = Integer.parseInt(req.getParameter("itemId"));

        itemService.markItemInCart(itemId);

        out.println(new ObjectMapper().writeValueAsString(new Response("item is in cart ",true,null)));
    }

    public void deleteItem(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        Integer itemId = Integer.parseInt(req.getParameter("itemId"));

        itemService.deleteItem(itemId);

        out.println(new ObjectMapper().writeValueAsString(new Response("item has been deleted ",true,null)));

    }
}
